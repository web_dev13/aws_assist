import { Module } from '@nestjs/common';
import { MenuQueueService } from './menu-queue.service';
import { MenuQueueController } from './menu-queue.controller';
import { MenuQueue } from './entities/menu-queue.entity';
import { OrderItem } from 'src/orders/entities/order-item';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Menu } from 'src/menus/entities/menu.entity';
import { Order } from 'src/orders/entities/order.entity';

@Module({
  imports: [TypeOrmModule.forFeature([MenuQueue, OrderItem, Menu, Order])],
  controllers: [MenuQueueController],
  providers: [MenuQueueService],
})
export class MenuQueueModule {}
