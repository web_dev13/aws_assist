import { Test, TestingModule } from '@nestjs/testing';
import { MenuQueueService } from './menu-queue.service';

describe('MenuQueueService', () => {
  let service: MenuQueueService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MenuQueueService],
    }).compile();

    service = module.get<MenuQueueService>(MenuQueueService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
