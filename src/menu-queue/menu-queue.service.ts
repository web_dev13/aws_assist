import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateMenuQueueDto } from './dto/create-menu-queue.dto';
import { UpdateMenuQueueDto } from './dto/update-menu-queue.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { MenuQueue } from './entities/menu-queue.entity';
import { Repository } from 'typeorm';
import { Menu } from 'src/menus/entities/menu.entity';
import { OrderItem } from 'src/orders/entities/order-item';
import { Order } from 'src/orders/entities/order.entity';

@Injectable()
export class MenuQueueService {
  constructor(
    @InjectRepository(MenuQueue)
    private menuQueueRepository: Repository<MenuQueue>,
    @InjectRepository(Menu)
    private menuRepository: Repository<Menu>,
    @InjectRepository(OrderItem)
    private orderItemRepository: Repository<OrderItem>,
    @InjectRepository(Order)
    private orderRepository: Repository<Order>,
  ) {}

  async create(createMenuQueueDto: CreateMenuQueueDto) {
    const orderItem = await this.orderItemRepository.findOne({
      where: { id: createMenuQueueDto.orderItemId },
      relations: ['menu', 'order'],
    });
    console.log(orderItem);

    const menu = await this.menuRepository.findOneBy({
      id: orderItem.menu.id,
    });
    const menuQueue: MenuQueue = new MenuQueue();
    menuQueue.menuStatus = createMenuQueueDto.menuStatus;
    menuQueue.menu = menu;
    menuQueue.orderItem = orderItem;
    return this.menuQueueRepository.save(menuQueue);
    // return menuQueue;
  }

  findAll() {
    return this.menuQueueRepository.find({
      relations: [
        'menu',
        'orderItem',
        'orderItem.order',
        'orderItem.order.table',
      ],
    });
  }
  findByODItem(odItemID: number) {
    // return this.menuQueueRepository.find({
    //   relations: [
    //     'menu',
    //     'orderItem',
    //     'orderItem.order',
    //     'orderItem.order.table',
    //   ],
    // });
    return this.orderItemRepository.findOne({
      where: { id: odItemID },
      relations: { menuQueues: true },
    });
  }

  findOne(id: number) {
    return this.menuQueueRepository.findOne({
      where: { id },
      relations: [
        'menu',
        'orderItem',
        'orderItem.order',
        'orderItem.order.table',
      ],
    });
  }

  async update(id: number, updateMenuQueueDto: UpdateMenuQueueDto) {
    const menuQueue = await this.menuQueueRepository.findOne({
      where: { id: id },
      relations: { orderItem: true },
    });
    if (!menuQueue) {
      throw new NotFoundException();
    }
    const updateMenuQueue = { ...menuQueue, ...updateMenuQueueDto };
    await this.menuQueueRepository.save(updateMenuQueue);
    // console.log(menuQueue);
    const orderItem = await this.orderItemRepository.findOne({
      where: { id: menuQueue.orderItem.id },
      relations: { menuQueues: true, order: true },
    });
    console.log(orderItem);
    // let order = await this.orderRepository.findOne({
    //   where: { id: orderItem.order.id },
    //   relations: { orderItems: true },
    // });
    if (
      orderItem.menuQueues.filter((item) => item.menuStatus.includes('Kitchen'))
        .length > 0
    ) {
      orderItem.status = 'Kitchen';
      console.log(orderItem.status);
      // order.status = 'Kitchen';
    } else if (
      orderItem.menuQueues.filter((item) =>
        item.menuStatus.includes('Ready to serve'),
      ).length > 0
    ) {
      orderItem.status = 'Ready to serve';
    } else if (
      orderItem.menuQueues.filter((item) => item.menuStatus.includes('Served'))
        .length > 0
    ) {
      orderItem.status = 'Complete';
    } else if (
      orderItem.menuQueues.filter((item) =>
        item.menuStatus.includes('In process'),
      ).length > 0
    ) {
    } else if (
      orderItem.menuQueues.filter((item) => item.menuStatus.includes('Cancel'))
        .length == orderItem.menuQueues.length
    ) {
      orderItem.status = 'Cancel';
    }
    await this.orderItemRepository.save(orderItem);
    // await this.orderRepository.save(order);
    const order = await this.orderRepository.findOne({
      where: { id: orderItem.order.id },
      relations: { orderItems: true },
    });
    // check order status base on orderItem
    if (
      order.orderItems.filter((item) => item.status.includes('Kitchen'))
        .length > 0
    ) {
      console.log('enter kitchen');
      order.status = 'Kitchen';
    } else if (
      order.orderItems.filter((item) => item.status.includes('Complete'))
        .length > 0
    ) {
      console.log('enter Complete');
      order.status = 'Wait for payment';
    } else if (
      order.orderItems.filter((item) => item.status.includes('Cancel'))
        .length == order.orderItems.length
    ) {
      console.log('enter Cancel');
      order.status = 'Cancel';
    }
    console.log(order.status);

    await this.orderRepository.save(order);

    console.log(orderItem.status);

    console.log(updateMenuQueue);
    updateMenuQueue.orderItem.status = orderItem.status;

    return this.menuQueueRepository.save(updateMenuQueue);
    // return 'update work';
  }
  async updateODItem(id: number, updateMenuQueueDto: UpdateMenuQueueDto) {
    const orderItem = await this.orderItemRepository.findOne({
      where: { id: id },
      relations: ['menuQueues'],
    });

    return this.orderItemRepository.save(orderItem);
    // return orderItem;
  }

  async remove(id: number) {
    const menuQueue = await this.menuQueueRepository.findOne({
      where: { id: id },
    });
    if (!menuQueue) {
      throw new NotFoundException();
    }

    return this.menuQueueRepository.softRemove(menuQueue);
  }
}
