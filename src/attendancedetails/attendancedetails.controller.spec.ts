import { Test, TestingModule } from '@nestjs/testing';
import { AttendancedetailsController } from './attendancedetails.controller';
import { AttendancedetailsService } from './attendancedetails.service';

describe('AttendancedetailsController', () => {
  let controller: AttendancedetailsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AttendancedetailsController],
      providers: [AttendancedetailsService],
    }).compile();

    controller = module.get<AttendancedetailsController>(
      AttendancedetailsController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
