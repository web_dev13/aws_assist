import { Employee } from 'src/employees/entities/employee.entity';

export class CreateAttendancedetailDto {
  employee: Employee;

  workStart: string;

  workOff: string;

  wage: number;

  status: string;
}
