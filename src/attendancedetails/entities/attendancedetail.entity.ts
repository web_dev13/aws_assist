import { AttendanceReport } from 'src/attendance-reports/entities/attendance-report.entity';
import { Employee } from 'src/employees/entities/employee.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class AttendanceDetail {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Employee, (employee) => employee.attendanceDetails)
  employee: Employee;

  @Column()
  workStart: string;

  @Column()
  workOff: string;

  @Column({ type: 'float' })
  wage: number;

  @Column()
  status: string;

  @ManyToOne(
    () => AttendanceReport,
    (attendanceReport) => attendanceReport.attendanceDetails,
  )
  attendanceReport: AttendanceReport;

  @CreateDateColumn()
  createAt: Date;

  @UpdateDateColumn()
  updateAt: Date;

  @DeleteDateColumn()
  deleteAt: Date;
}
