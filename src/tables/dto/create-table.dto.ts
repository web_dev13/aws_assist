import { IsInt, IsNotEmpty, Min } from 'class-validator';

export class CreateTableDto {
  @IsNotEmpty()
  @IsInt({ message: 'Max seat values must be an integer.' })
  @Min(0, { message: 'Max seat values cant be less than "0".' })
  maxSeat: number;

  @IsNotEmpty()
  status: string;

  @IsNotEmpty()
  qrCodeSource: string;
}
