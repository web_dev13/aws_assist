import {
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateMenuDto } from './dto/create-menu.dto';
import { UpdateMenuDto } from './dto/update-menu.dto';
import { Menu } from './entities/menu.entity';

@Injectable()
export class MenusService {
  constructor(
    @InjectRepository(Menu)
    private menusRepository: Repository<Menu>,
  ) {}

  async create(createMenuDto: CreateMenuDto) {
    const { name } = createMenuDto;
    const menuExists = await this.nameExists(name);
    if (menuExists) {
      throw new ConflictException('Menu with this name already exists');
    }
    const material = this.menusRepository.create(createMenuDto);
    return this.menusRepository.save(material);
  }

  findAll() {
    return this.menusRepository.find({
      relations: {
        menuQueues: true,
      },
    });
  }

  findOne(id: number) {
    return this.menusRepository.findOne({
      where: { id },
      relations: {
        menuQueues: true,
      },
    });
  }

  async findOneByName(name: string) {
    return this.menusRepository.findOne({
      where: { name: name },
    });
  }

  async update(id: number, updateMenuDto: UpdateMenuDto) {
    const refMenu = await this.menusRepository.findOneBy({ id });
    if (!refMenu) {
      throw new NotFoundException();
    }
    const updatedMaterial = { ...refMenu, ...updateMenuDto };
    return this.menusRepository.save(updatedMaterial);
  }

  async remove(id: number) {
    const refMenu = await this.menusRepository.findOneBy({ id });
    if (!refMenu) {
      throw new NotFoundException();
    }
    return this.menusRepository.softRemove(refMenu);
  }

  async nameExists(name: string): Promise<boolean> {
    const menu = await this.menusRepository.findOne({
      where: { name },
    });
    return !!menu;
  }
}
